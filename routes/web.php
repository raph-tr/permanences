<?php

use App\Http\Controllers\PermanenceController;
use App\Models\Permanence;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/******
Route::get('/', function () {
    $futurePermanences = Permanence::orderBy('start', 'ASC')
        ->where('start', '>', now() )
        ->get();
    $pastPermanences = Permanence::orderBy('start', 'DESC')
        ->where('start', '<', now() )
        ->get();
    return view('home', compact('futurePermanences','pastPermanences') );
})->name('home');
 * ******/
Route::get('/', [PermanenceController::class, 'index'])->name('home');

/*
 * Permanence routes
 */
Route::get('/permanence/dashboard',  [PermanenceController::class, 'dashboard'])->middleware(['auth'])->name('permanence.dashboard');
Route::get('/permanence/create',  [PermanenceController::class, 'create'])->middleware(['auth'])->name('permanence.create');
Route::post('/permanence/create', [PermanenceController::class, 'store'])->middleware(['auth'])->name('permanence.store');
Route::patch('/permanence/edit', [PermanenceController::class, 'update'])->middleware(['auth'])->name('permanence.update');
Route::get('/permanence/edit/{id:uuid}', [PermanenceController::class, 'edit'])->middleware(['auth'])->name('permanence.edit');
Route::get('/permanence/delete/{id:uuid}', [PermanenceController::class, 'delete'])->middleware(['auth'])->name('permanence.delete');

Route::get('/calendar', [PermanenceController::class, 'showCalendar'] )->name('calendar');;


/*
 * Static pages routes
 */
Route::get('/a-propos', function (){ return view('a-propos'); })->name('a-propos');
Route::get('/l-equipe', function (){ return view('equipe'); })->name('equipe');
Route::get('/contact', function (){ return view('contact'); })->name('contact');
Route::get('/index', function (){ return view('index'); });




require __DIR__.'/auth.php';
