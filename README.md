## Permanences : appli de gestion et affichage des présences des membres d'une asso aux permanences qu'ils assurent.

Le site doit permettre :
- aux membres d'une asso de gérer (CRUD) leurs présences aux permanences de l'asso
- au 'visiteur' de savoir quand et par qui une permanence est assurée


# CdC
- basique
  - pouvoir se logguer en tant que membre et gérer ses présences aux permanences
  - afficher au public les horaires des permanences assurées et par qui elles le sont
  

- fonctions avançées
  - evenements : gérer des evenements particuliers en plus des simples permanences
  - gestion admin :
    - personnaliser les données de l'asso (image, nom, adresse, description, etc..)
    - définir ou pas les créneaux de permanences possibles
    - gérer les membres
  - notification des permanences ouvertes sur smartphones pour clients ?
  - notification des permanences à assurer dans la journée pour les bénévoles concernés
  - email de rappel le dimanche soir / la veille / le matin aux benevoles si permanence dans la journée
  - plusieurs locaux possibles pour les permanences
  - multilingual app ?


- messagerie ?
  - messages entre bénévoles pour centraliser les remarques ?
