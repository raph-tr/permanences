<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePermanenceRequest;
use App\Http\Requests\UpdatePermanenceRequest;
use App\Models\Permanence;
use App\Models\User;
use http\Env\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Acaronlex\LaravelCalendar\Calendar;


class PermanenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $futurePermanences = Permanence::orderBy('start', 'ASC')
            ->where('start', '>', now() )
            ->get();
        $pastPermanences = Permanence::orderBy('start', 'DESC')
            ->where('start', '<', now() )
            ->get();
        $calendar = $this->calendar();

        return view('home', compact('futurePermanences','pastPermanences', 'calendar') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permanences.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePermanenceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePermanenceRequest $request)
    {
        // validation
        //dd($request);
        $this->validate($request, [
            'day' => 'required',
            'start' => 'required',
            'duration' => 'required',
        ]);

        // save in db
        $start = new Carbon($request->day.' '.$request->start);
        $end = new Carbon($request->day.' '.$request->start);  // end = start
        $end = $end->addMinutes($request->duration);  //  end = end+duration

        Permanence::create([
            "start" => $start,
            "end" => $end,
            "user_id" => Auth::user()->id
        ]);

        // redirect
        return redirect(route('permanence.dashboard'));
    }

    /**
     * Display the user resources list.
     *
     * @param  \App\Models\Permanence  $permanence
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Permanence $permanence)
    {
        // select user's permanences
        $futurePermanences = Permanence::orderBy('start', 'ASC')
            ->where('start', '>', now() )
            ->where('user_id', '=', Auth::user()->id)
            ->get();
        $pastPermanences = Permanence::orderBy('start', 'DESC')
            ->where('start', '<', now() )
            ->get();
        return view('permanences.memberDashboard', compact('futurePermanences','pastPermanences') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Permanence  $permanence
     * @return \Illuminate\Http\Response
     */
    public function show(Permanence $permanence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Permanence  $permanence
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        //
        $permanence = Permanence::findOrFail($id);
        //dd($permanence);
        $id = $permanence->id;
        $userId = $permanence->user_id;
        $day = date('Y-m-d', strtotime($permanence->start));
        $start = date('H:i', strtotime($permanence->start));
        $duration = $duration = 60*date_diff(date_create($permanence->end),(date_create($permanence->start)))->h
            +  date_diff(date_create($permanence->end),(date_create($permanence->start)))->i ;

        if ($permanence->user_id == Auth::user()->id or Auth::user()->is_admin) {
            return view('permanences.edit')->with(['id'=>$id, 'day'=>$day, 'start'=>$start, 'duration'=>$duration]);
        } else {
            return redirect()->route('permanence.dashboard');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePermanenceRequest  $request
     * @param  \App\Models\Permanence  $permanence
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePermanenceRequest $request, Permanence $permanence)
    {
        $permanence = Permanence::findOrFail($request->id);

        if ($permanence->user_id == Auth::user()->id or Auth::user()->is_admin) {
            $start = new Carbon($request->day . ' ' . $request->start);
            $end = new Carbon($request->day . ' ' . $request->start);  // end = start
            $end = $end->addMinutes($request->duration);  //  end = end+duration

            $permanence->start = $start;
            $permanence->end = $end;
            $permanence->save();
        }
        return redirect()->route('permanence.dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Permanence  $permanence
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {

        $permanence = Permanence::findOrFail($id);
        if ($permanence->user_id == Auth::user()->id or Auth::user()->is_admin) {
            $permanence->delete();
        }
        return redirect()->route('permanence.dashboard');
    }

    /**
     * Show calendar
     *
     */
    public function calendar()
    {
        $events = [];

        $permanences = Permanence::orderBy('start', 'ASC')
            ->get();

        foreach($permanences as $permanence) {

            $options = [];
            $title = '';

            if (Auth::user() and Auth::user()->id == $permanence['user_id'] ) {
                $options['url'] = route('permanence.edit', ['id' => $permanence->id ]);
                $options['backgroundColor'] = "#FAC14D";
                $options['textColor'] = "#343531";
                $options['borderColor'] = "darkred";
                $title = 'edit';
            }

            $events[] = \Calendar::event(
                $title, //$permanence['user']['name'], //event title
                0, //full day event?
                $permanence['start'], //start time (you can also use Carbon instead of DateTime)
                $permanence['end'], //end time (you can also use Carbon instead of DateTime)
                $permanence['id'], //optionally, you can specify an event ID
                $options
            );
        }

        $calendar = new Calendar();

        $calendar->addEvents($events);

        $calendar->setOptions([
            'themeSystem'=> 'bootstrap',
            'timeZone'=> 'Europe/Paris',
            'locale' => 'fr',
            'firstDay' => 1,
            'eventDisplay' => 'block',
            // 'eventBackgroundColor' => 'lightgreen',
            'displayEventTime' => true,
            'displayEventEnd' => true,
            'selectable' => true,
            'initialView' => 'dayGridMonth',
            'headerToolbar' => [
                'left' => 'prev next today',
                'center' => 'title',
                'right' => ''
            ],
            'buttonText' => [
                  'prev'=> 'prec.',
                  'next'=> 'suiv.',
                  'today' => 'maintenant'
           ]
        ]);

        $calendar->setId('1');

        $calendar->setCallbacks([
            'select' => 'function(selectionInfo){
            }',
            'eventClick' => 'function(event){
            }'
        ]);

        return $calendar;
    }

    public function showCalendar(){
        $calendar = $this->calendar();
        return view('calendar', compact('calendar'));
    }

}
