<?php

namespace App\Http\Livewire;

use App\Models\Permanence;
use Livewire\Component;


class PermanencesCalendar extends LivewireCalendar {

    public function events() : Collection
    {
        // must return a Laravel collection
        $model = Model::query()
            ->whereDate('start', '>=', $this->gridStartsAt)
            ->whereDate('start', '<=', $this->gridEndsAt)
            ->get();

        return Model::query()
            ->whereDate('start', '>=', $this->gridStartsAt)
            ->whereDate('start', '<=', $this->gridEndsAt)
            ->get()
            ->map(function (Model $model) {
                return [
                    'id' => $model->id,
                    'title' => $model->title,
                    'description' => $model->description,
                    'date' => $model->start
                ];
            });
    }
}
