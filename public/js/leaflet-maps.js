var map = L.map('map').setView([45.26381, 5.56656], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([45.26381, 5.56656]).addTo(map)
.bindPopup('C\'est là qu\'on est.<br> Viendez quand vous pouvez !.')
.openPopup();

