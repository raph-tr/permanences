<x-layout xmlns="http://www.w3.org/1999/html">

    <x-slot name="header">

    </x-slot>

    <x-slot name="main">
        <div  class="row justify-content-center align-items-center py-8 my-3">
          <div class="col-lg-6 py-3 wow fadeInUp">
            {!! $calendar->calendar() !!}
            {!! $calendar->script() !!}
          </div>
        </div>
    </x-slot>

</x-layout>
