<x-layout xmlns="http://www.w3.org/1999/html">

    <x-slot name="header">
    </x-slot>

    <x-slot name="main">
        <div>
            <livewire:permanences-calendar/>
        </div>
    </x-slot>

</x-layout>
