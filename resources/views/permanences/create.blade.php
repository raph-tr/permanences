<x-layout>
    <x-slot name="main">
    </x-slot>

    <x-slot name="header">
        <div class="page-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 py-3 wow zoomIn">
                        <h1>I'll be there !!! </h1>
                        <a href="{{ route('home') }}" class="small">Finalement, en fait, non je serai pas là...</a>
                    </div>
                    <div class="col-lg-6 py-3  wow fadeInRight">
                          <form method="POST" action="{{ route('permanence.store') }}" >
                            <!-- CSRF -->
                            @csrf

                           <div class="form-group">
                                <label for="day" >
                                    <div class="contact-list">
                                        <div class="icon"><span class="mai-calendar"></span></div>
                                        <div class="content">Quel jour ?</div>
                                    </div>
                                </label><br/>
                                <input class="form-control" type="date" name="day" id="day" value="{{ date('Y-m-d', strtotime(now())) }}" />
                            </div>

                           <div class="form-group">
                                <label for="start" >
                                   <div class="contact-list">
                                       <div class="icon"><span class="mai-watch"></span></div>
                                       <div class="content">À partir de quelle heure  ?</div>
                                   </div>
                           </label><br/>
                                <select name="start" id="start" class="form-control">
@for ($i=8; $i<21; $i++ )
                                        <option value="{{$i}}:00">{{$i}}h00</option>
                                        <option value="{{$i}}:30" @if($i==16) selected @endif>{{$i}}h30</option>
@endfor
                                </select>
                           </div>
                           <div class="form-group">
                                <label for="duration">
                                    <div class="contact-list">
                                            <div class="icon"><span class="mai-timer"></span></div>
                                            <div class="content">Pour combien de temps ?</div>
                                   </div>
                                </label> <br/>
                                <select name="duration" id="duration" class="form-control">
                                    <option value="60" selected>1h00'</option>
                                    <option value="90">1h30'</option>
                                    <option value="120">2h00'</option>
                                    <option value="150">2h30'</option>
                                    <option value="180">3h00'</option>
                                    <option value="210">3h30'</option>
                                    <option value="240">4h00'</option>
                                </select>
                            </div>

                            <button type="submit" name="submit" class="btn btn-success">Je tiens le bar !</button>

                        </form>
                    </div>
                  </div>
            </div>
        </div>
    </x-slot>
</x-layout>
