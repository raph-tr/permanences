<x-layout>
    <x-slot name="main">
    </x-slot>

    <x-slot name="header">
        <div class="page-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 py-3 wow zoomIn">
                        <h1>I'll be there !!! </h1>
                        <a href="{{ route('home') }}" class="small">Finalement, en fait, non je serai pas là...</a>
                    </div>
                    <div class="col-lg-6 py-3  wow fadeInRight">
                        <form method="POST" action="{{ route('permanence.update') }}" >
                            @method('PATCH')

                            <!-- CSRF -->
                            @csrf
                                <h4>Permanence du {{ $day }}, à {{ $start}} pendant {{ $duration }} min.</h4>

                            <input type="hidden" name="id" value="{{ $id }}" />

                            <div class="form-group">
                                <label for="day" >
                                    <div class="contact-list">
                                        <div class="icon"><span class="mai-calendar"></span></div>
                                        <div class="content">Quel jour ?</div>
                                    </div>
                                </label><br/>

                                <input class="form-control" type="date" name="day" id="day" value="{{ $day }}" />
                            </div>

                            <div class="form-group">
                                <label for="start" >
                                    <div class="contact-list">
                                        <div class="icon"><span class="mai-watch"></span></div>
                                        <div class="content">À partir de quelle heure  ?</div>
                                    </div>
                                </label><br/>
                                <select name="start" id="start" class="form-control">
                                    @for ($h=8; $h<21; $h++ )
                                        <option value="{{$h}}:00" @if( $h.":00" == $start ) selected @endif>{{$h}}h00</option>
                                        <option value="{{$h}}:30" @if( $h.":30" == $start ) selected @endif>{{$h}}h30</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="duration">
                                    <div class="contact-list">
                                        <div class="icon"><span class="mai-timer"></span></div>
                                        <div class="content">Pour combien de temps ?</div>
                                    </div>
                                </label> <br/>
                                <select name="duration" id="duration" class="form-control">
                                    @for ($d=60; $d<250; $d+=30)
                                    <option value="{{$d}}" @if ($duration == $d) selected @endif>{{ floor($d/60) }}h{{ $d%60 }}'</option>
                                    @endfor
                                </select>
                            </div>

                            <button type="submit" name="submit" class="btn btn-success">Je tiens le bar !</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout>
