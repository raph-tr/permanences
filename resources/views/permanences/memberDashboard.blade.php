<x-layout>
    <x-slot name="header">
    </x-slot>
    <x-slot name="main">
        <div class="page-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12 py-3">
                        <h2>Mes permanences à venir</h2>
                        <ul>
                            @foreach( $futurePermanences as $permanence )
                                <li>{{ $permanence->start }}
                                    <a href="{{ route('permanence.edit', ['id' => $permanence->id ]) }}"><span class="icon"><span class="mai-pencil"></span> modifier</span></a>
                                    <a href="{{ route('permanence.delete', ['id' => $permanence->id ]) }}"><span class="icon"><span class="mai-trash-bin"></span>supprimer </span></a>
                                    <a href="#"><span class="icon"><span class="mai-add-circle"></span>ajouter un commentaire</span></a>
                                </li>
                            @endforeach
                        </ul>
                        <h2>Mes permanences passées</h2>
                        <ul>
                            @foreach( $pastPermanences as $permanence )
                                <li>{{ $permanence->start }}
                                    <a href="#"><span class="icon"><span class="mai-add-circle"></span>ajouter un commentaire</span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </x-slot>
</x-layout>
