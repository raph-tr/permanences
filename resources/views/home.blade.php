<x-layout xmlns="http://www.w3.org/1999/html">
    <x-slot name="header">
        <div class="container mt-5">
            <div class="">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-6 text-center">
                        <div class="counter-section btn-success rounded-pill my-3 py-2">
                            <strong>Prochaine ouverture : {{ date('\l\e d/m/y, \à H:s', strtotime($futurePermanences[0]->start)) }},</strong>
                            <div class="counter"  id="init_clock"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @auth
            <div class="row justify-content-center align-items-center py-8 my-3 ">
                <a href="{{ route('permanence.create') }}" class="my-8">
                    <button class="btn btn-warning rounded-pill ">
                        <strong>Poser une permanence.</strong>
                    </button>
                </a>
            </div>
        @endauth
    </x-slot>

    <x-slot name="main">
        <div  class="row justify-content-center align-items-center py-8 my-3">
            <div class="col-lg-6 py-3 wow fadeIn">
                {!! $calendar->calendar() !!}
                {!! $calendar->script() !!}
            </div>
        </div>
        <div class="page-banner home-banner">
            <div class="container h-100">
                <div class="row align-items-center h-100">
                    <div class="col-lg-7 py-3 wow fadeInUp">
                        <h2 class="title-section">Prochaines ouvertures&nbsp;:</h2>
                        <div class="divider"></div>
                        <ul>

                            @foreach($futurePermanences as $permanence)
                                <li>
                                    @auth
                                        @if ($permanence->user_id == Auth::user()->id )
                                            <a href="{{ route('permanence.dashboard') }}" title="dashboard">
                                        @endif
                                    @endauth
                                    le {{ date('d/m/y, \d\e H:i', strtotime($permanence->start)) }} à {{ date('H:i', strtotime($permanence->end)) }}, par {{ $permanence->user->name }}
                                    @auth
                                        @if ( $permanence->user_id == Auth::user()->id )
                                            </a>
                                        @endif
                                    @endauth
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-lg-5 py-3 wow zoomIn">
                        <h2 class="title-section">Évènements à venir&nbsp;:</h2>
                        <div class="divider"></div>
                        <ul>
                            @foreach($pastPermanences as $permanence)
                                @auth
                                    @if ($permanence->user_id == Auth::user()->id )
                                        <a href="{{ route('permanence.dashboard') }}" title="dashboard">
                                    @endif
                                @endauth
                                <li>
                                    {{ date('d/m/y, \d\e H:i', strtotime($permanence->start)) }} à {{ date('H:i', strtotime($permanence->end)) }}, par {{ $permanence->user->name }}
                                    @auth
                                        @if ( $permanence->user_id == Auth::user()->id )
                                            </a>
                                        @endif
                                    @endauth
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"></script>
<script>
    $('#init_clock').countdown('{{ $futurePermanences[0]->start }}', function(event) {
        var $this = $(this).html(event.strftime(''
            + 'soit dans <span><span class="days">%D</span> jours, </span> '
            + '<span><span class="hours">%H</span>h </span> '
            + '<span><span class="minutes">%M</span>\' </span> '
            + '<span><span class="seconds">%S</span>\'\' ;)</span> '));
    });
</script>
