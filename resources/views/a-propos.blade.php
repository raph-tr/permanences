
<x-layout xmlns="http://www.w3.org/1999/html">

    <x-slot name="header">
        <div class="page-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 py-3 wow fadeInDown">
                        <h2 class="title-section">À propos de nous</h2>
                        <div class="divider"></div>
                        <p>
                            Une buvette associative à Montaud. <br />
                            Tenue quand ils peuvent par des villageois bénévoles.<br />
                            Aux horaires donc forcément aléatoires ;)
                        </p>

                        <ul class="contact-list">
                            <li>
                                <div class="icon"><span class="mai-map"></span></div>
                                <div class="content">Les horaires sur notre site</div>
                            </li>
                            <li>
                                <div class="icon"><span class="mai-mail"></span></div>
                                <div class="content"><a href="#">info@montaubar.fr</a></div>
                            </li>
                            <li>
                                <div class="icon"><span class="mai-phone-portrait"></span></div>
                                <div class="content"><a href="#">+00 1122 3344 55</a></div>
                            </li>
                        </ul>
                        <div id="map"></div>
                    </div>
                    <div class="col-lg-6 py-3  wow fadeInRight">
                        <div class="subhead">Contact Us</div>
                        <h2 class="title-section">Drop Us a Line</h2>
                        <div class="divider"></div>

                        <form action="#">
                            <div class="py-2">
                                <input type="text" class="form-control" placeholder="Full name">
                            </div>
                            <div class="py-2">
                                <input type="text" class="form-control" placeholder="Email">
                            </div>
                            <div class="py-2">
                                <textarea rows="6" class="form-control" placeholder="Enter message"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary rounded-pill mt-4">Send Message</button>
                        </form>
                    </div>
                </div>
            </div> <!-- .container -->
        </div> <!-- .page-section -->
    </x-slot>

    <x-slot name="main">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
              integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
              crossorigin=""/>
        <style>
            #map {
                height: 200px;
                width: 100%;
            }
        </style>

        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
                integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
                crossorigin=""></script>
    </x-slot>

</x-layout>
