<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <a href="{{ route('home') }}" class="navbar-brand">Mont'au<span class="text-primary">Bar</span></a>

        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbarContent">
            <ul class="navbar-nav ml-lg-4 pt-3 pt-lg-0">
                <li class="nav-item {{ (request()->route()->named('home')) ? 'active' : '' }}">
                    <a href="{{ route('home') }}" class="nav-link">Accueil</a>
                </li>
                <li class="nav-item {{ (request()->route()->named('a-propos')) ? 'active' : '' }}">
                    <a href="{{ route('a-propos') }}" class="nav-link">À propos</a>
                </li>
                <li class="nav-item {{ (request()->route()->named('equipe')) ? 'active' : '' }}">
                    <a href="{{ route('equipe') }}" class="nav-link">L'équipe</a>
                </li>
                <li class="nav-item">
                    <a href="blog.html" class="nav-link">Blog</a>
                </li>
                <li class="nav-item {{ (request()->route()->named('calendar')) ? 'active' : '' }}">
                    <a href="{{ route('calendar') }}" class="nav-link">Calendrier</a>
                </li>
            </ul>

            <div class="ml-auto">
                @auth
                    <div class="dropdown">
                        <button  class="btn btn-primary rounded-pill dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->name }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a href="{{ route('permanence.create') }}" class="dropdown-item btn-success">Tenir le bar</a>
                            <a href="{{ route('permanence.dashboard') }}" class="dropdown-item ">Mon tableau de bord</a>

                            <form method="POST" action="{{ route('logout') }}"> @csrf
                                <a href=" {{ route('logout') }}"
                                                 onclick="event.preventDefault();
                                                    this.closest('form').submit();"
                                   class="dropdown-item">
                                    Déconnexion
                                </a>
                            </form>
                        </div>
                    </div>
                @else
                    <div class="dropdown">
                        <button  class="btn btn-primary rounded-pill dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Connexion
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a href="{{ route('login') }}" class="dropdown-item">Se connecter</a>
                            <a href="{{ route('register') }}" class="dropdown-item">S'inscrire</a>
                        </div>
                    </div>
                @endauth
            </div>
        </div>
    </div>
</nav>
